# Controls
Controls:

| Tecles | Acció |
| ------ | ------ |
|🡠/🡡/🡣/🡢      |`Moure el personatge pel món`  |
|E               |`Atacar`            |
|Botó dret del ratolí               |`Moure la càmera lliurement`            |

# Gameplay

El nivell consta de 4 parts:
- En la primera part hi ha unes plataformes que es van movent i el personatge ha de saltar en elles.
- En la segona part hi ha 3 pèndols que el personatge ha d'esquivar. Si el pèndol xoca amb el personatge, aquest surt disparat cap els costats.
- En la tercera part, el personatge ha de saltar en plataformes que estan ocultes però que es van mostrant poc a poc.
- En la quarta part, el personatge ha de colocar les 4 pilotes de cada color en el seu lloc on les pertoca.

# Ampliacions
- Joins (hinge joint, spring), materials (glow effect, efecte de mirall), managers...
