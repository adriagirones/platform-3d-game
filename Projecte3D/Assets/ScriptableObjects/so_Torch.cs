﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Torch : ScriptableObject
{
    public float range;
    public Color color;

    public float maxReduction;
    public float maxIncrease;
    public float rateDamping;
    public float strength;
}
