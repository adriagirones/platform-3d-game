﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_BallCheckEvent : ScriptableObject
{
    private readonly List<scr_CheckBallEventListener> eventListeners = new List<scr_CheckBallEventListener>();

    // Start is called before the first frame update
    public void RaiseEvent(string ball, bool result)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(ball, result);
    }

    public void RegisterListener(scr_CheckBallEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(scr_CheckBallEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
