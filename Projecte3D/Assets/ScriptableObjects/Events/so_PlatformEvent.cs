﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_PlatformEvent : ScriptableObject
{
    private readonly List<scr_Zone3Manager> eventListeners = new List<scr_Zone3Manager>();

    // Start is called before the first frame update
    public void RaiseEvent(GameObject ID)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(ID);
    }

    public void RegisterListener(scr_Zone3Manager listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(scr_Zone3Manager listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
