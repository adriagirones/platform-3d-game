﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Time : ScriptableObject
{
    public float actualTime;
}
