﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scr_EndMenu : MonoBehaviour
{
    public GameObject textTime; 
    public so_Time time;

    void Start()
    {
        int min = (int) (time.actualTime / 60);
        int sec = (int)(time.actualTime % 60);
        textTime.GetComponent<TMPro.TextMeshProUGUI>().text = min+":"+sec;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
            SceneManager.LoadScene("MainScene");
    }
}
