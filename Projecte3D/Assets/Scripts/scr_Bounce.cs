﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Bounce : MonoBehaviour
{
    public float force;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			other.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
			Vector3 v = (this.transform.position - other.transform.position).normalized;
			if(v.x < 0f)
            {
				other.gameObject.GetComponent<Rigidbody>().AddForce(1000f, 0, 0);
			}
            else
            {
				other.gameObject.GetComponent<Rigidbody>().AddForce(-1000f, 0, 0);
			}
		}
	}
}
