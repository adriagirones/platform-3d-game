﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CheckBallEventListener : MonoBehaviour
{
    public so_BallCheckEvent Event;

    public GameObject teleport;

    bool blue = false;
    bool red = false;
    bool yellow = false;
    bool green = false;

    private void Start()
    {
        teleport.SetActive(false);
    }

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(string color, bool check)
    {
        if (color == "BlueBall")
            blue = check;
        else if (color == "RedBall")
            red = check;
        else if (color == "YellowBall")
            yellow = check;
        else if (color == "GreenBall")
            green = check;

        if (blue && red && green && yellow)
            teleport.SetActive(true);
        else
            teleport.SetActive(false);

    }
}
