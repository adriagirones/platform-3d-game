﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CharacterController : MonoBehaviour
{
    // Start is called before the first frame update
    Animator anim;
    bool isGrounded;
    bool canMove;
    Rigidbody rb;
    public SphereCollider attackCollider;

    void Start()
    {
        anim = this.GetComponentInChildren<Animator>();
        rb = this.GetComponent<Rigidbody>();
        canMove = true;
        attackCollider.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            if (Input.GetKeyDown("space") && isGrounded)
            {
                StartCoroutine(JumpFox());
            }

            if (Input.GetKeyUp("w"))
            {
                anim.SetInteger("StateMachine", 0);
            }

            if (Input.GetKeyUp("e"))
            {
                anim.SetInteger("StateMachine", 3);
                canMove = false;
                attackCollider.enabled = true;
                StartCoroutine(canMoveAgain());
            }
        }

    }

    IEnumerator canMoveAgain()
    {
        float lenghtanimation = anim.GetCurrentAnimatorClipInfo(0).Length - 0.2f;
        yield return new WaitForSeconds(lenghtanimation);
        anim.SetInteger("StateMachine", 0);
        canMove = true;
        //print("canmove");
        attackCollider.enabled = false;
    }

    private void FixedUpdate()
    {
        if (rb.velocity.magnitude >= maxVel)
            return;

        if (canMove)
        {
            if (Input.GetKey("w"))
            {
                if (isGrounded)
                {
                    anim.SetInteger("StateMachine", 1);


                }
                else
                    anim.SetInteger("StateMachine", 2);


                rb.AddForce(vel * this.transform.forward);
            }
            if (Input.GetKey("s"))
            {
                rb.AddForce(vel * -this.transform.forward);
            }
            if (Input.GetKey("a"))
            {
                this.transform.Rotate(new Vector3(0, -1, 0) * 3);
            }
            if (Input.GetKey("d"))
            {
                this.transform.Rotate(new Vector3(0, 1, 0) * 3);
            }
        }
       
        //print(rb.velocity);


    }

    public float vel;
    public float maxVel;

    private IEnumerator JumpFox()
    {
        anim.SetInteger("StateMachine", 2);
        yield return new WaitForSeconds(0f);
        isGrounded = false;
       // this.GetComponent<Rigidbody>().AddForce(this.transform.forward.x * 4f, this.transform.up.y * 8f, this.transform.forward.z * 2f, ForceMode.Impulse);
        this.GetComponent<Rigidbody>().AddForce(this.transform.up * 300);
      //  this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 200);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Terrain")
        {
            isGrounded = true;
            anim.SetInteger("StateMachine", 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Ball")
        {
         //   print("entra"+ this.transform.forward);
            Vector3 v = (other.transform.position - this.transform.position).normalized;
            //other.GetComponent<Rigidbody>().AddForce(this.transform.forward * 5000f);
            other.GetComponent<Rigidbody>().AddForce(v * 5000f);
            //print(other.GetComponent<Rigidbody>().velocity);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Water")
        {
            isGrounded = false;
        }
    }

}
