﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Zone3Manager : MonoBehaviour
{
    public GameObject[] platforms;
    public so_PlatformEvent Event;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject p in platforms)
        {
            p.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            activateFirst();
        }
    }

    void activateFirst()
    {
        platforms[0].SetActive(true);
    }

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameObject ID)
    {
       // platforms[ID++].SetActive(true);
        ID.SetActive(true);
    }




}
