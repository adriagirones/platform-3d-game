﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_FlameEffect : MonoBehaviour
{
    public so_Torch torchSettings;

    private Light actualLight;
    private float baseIntensity;

    private void Start()
    {
        actualLight = GetComponent<Light>();
        actualLight.color = torchSettings.color;
        actualLight.range = torchSettings.range;
        baseIntensity = actualLight.intensity;
        StartCoroutine(DoFlicker());
    }

    private IEnumerator DoFlicker()
    {
        while (true)
        {
            actualLight.intensity = Mathf.Lerp(actualLight.intensity, Random.Range(baseIntensity - torchSettings.maxReduction, baseIntensity + torchSettings.maxIncrease), torchSettings.strength * Time.deltaTime);
            yield return new WaitForSeconds(torchSettings.rateDamping);
        }
    }
}
