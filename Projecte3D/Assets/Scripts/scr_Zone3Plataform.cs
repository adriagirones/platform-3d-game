﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Zone3Plataform : MonoBehaviour
{
    public GameObject next;
    public so_PlatformEvent actualEvent;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            actualEvent.RaiseEvent(next);
        }
    }
}
