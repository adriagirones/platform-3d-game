﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Pendulum : MonoBehaviour
{
    Quaternion start, end;

    [SerializeField, Range(0.0f, 360f)]
    private float angle = 60;

    [SerializeField, Range(0.0f, 5.0f)]
    private float speed = 3;

    [SerializeField, Range(0.0f, 10.0f)]
    private float startTime;


    void Start()
    {
        start = PendulumRotation(angle);
        end = PendulumRotation(-angle);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        startTime += Time.fixedDeltaTime;
        transform.rotation = Quaternion.Lerp(start, end, (Mathf.Sin(startTime * speed + Mathf.PI / 2) + 1f) / 2f);
    }

    Quaternion PendulumRotation(float angle)
    {
        Quaternion pendulumRotation = transform.rotation;
        float angleZ = pendulumRotation.eulerAngles.z + angle;

        if(angleZ > 100)
        {
            angleZ -= 360;
        }
        else if(angleZ < -100)
        {
            angleZ += 300;
        }
        pendulumRotation.eulerAngles = new Vector3(pendulumRotation.eulerAngles.x, pendulumRotation.eulerAngles.y, angleZ);
        return pendulumRotation;
    }

}
