﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Floater : MonoBehaviour
{
    public GameObject fox;
    Rigidbody rb;
    public float depthBeforeSubmerged;
    public float displacementAmount;
    bool flag;

    private void Start()
    {
        rb = fox.GetComponent<Rigidbody>();
        flag = false;
    }

    private void FixedUpdate()
    {
        if(fox.transform.position.y < this.gameObject.transform.position.y && flag)
        {
            float displacementMultiplier = Mathf.Clamp01(-fox.transform.position.y / depthBeforeSubmerged) * displacementAmount;
            rb.AddForce(new Vector3(0f, Mathf.Abs(Physics.gravity.y) * displacementMultiplier, 0f), ForceMode.Acceleration);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            flag = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            flag = false;
        }
    }


}
