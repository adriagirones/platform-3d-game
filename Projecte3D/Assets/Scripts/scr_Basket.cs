﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Basket : MonoBehaviour
{
    private bool destroyed;
    public GameObject stick1;
    public GameObject stick2;
    public GameObject basket;

    private void Start()
    {
        destroyed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!destroyed && other.gameObject.tag == "Player")
        {
            destroyed = true;
            stick1.GetComponent<Rigidbody>().AddForce(stick1.transform.up * 500);
            stick2.GetComponent<Rigidbody>().AddForce(stick1.transform.up * 500);
            basket.GetComponent<SpringJoint>().spring = 0;
            StartCoroutine(DestroyBasket());
        }
    }

    IEnumerator DestroyBasket()
    {
        yield return new WaitForSeconds(1f);
        Destroy(basket.gameObject);
        Destroy(stick1.gameObject);
        Destroy(stick2.gameObject);
    }
}
