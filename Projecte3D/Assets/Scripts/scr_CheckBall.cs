﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CheckBall : MonoBehaviour
{
    public string colorToCheck;
    public so_BallCheckEvent checkEvent;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == colorToCheck)
        {
            checkEvent.RaiseEvent(colorToCheck, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == colorToCheck)
        {
            checkEvent.RaiseEvent(colorToCheck, false);
        }
    }
}
