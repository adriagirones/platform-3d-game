﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_GameController : MonoBehaviour
{
    public Camera mainCamera;
    public Transform pj;
    public bool restartCam;

    private float sensitivityX = 300F;

    private float actualTime;

    // Start is called before the first frame update
    void Start()
    {
        restartCam = true;
        actualTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        MouseInput();

        if (Input.GetKey("w"))
        {
            restartCam = true;
        }
        actualTime += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (restartCam)
        {
            mainCamera.transform.position = new Vector3(
                 Mathf.Lerp(mainCamera.transform.position.x, pj.transform.position.x - pj.transform.forward.x * 3, Time.fixedDeltaTime * 5),
                 Mathf.Lerp(mainCamera.transform.position.y, pj.transform.position.y + 3, Time.fixedDeltaTime * 5),
                 Mathf.Lerp(mainCamera.transform.position.z, pj.transform.position.z - pj.transform.forward.z * 3, Time.fixedDeltaTime * 5)
            );
            mainCamera.transform.LookAt(new Vector3(pj.transform.position.x, pj.transform.position.y + 2, pj.transform.position.z));
        }
        else
        {
            mainCamera.transform.position = new Vector3(
                  mainCamera.transform.position.x,
                  Mathf.Lerp(mainCamera.transform.position.y, pj.transform.position.y + 2, Time.fixedDeltaTime * 15),
                  mainCamera.transform.position.z
             );
            mainCamera.transform.LookAt(new Vector3(pj.transform.position.x, pj.transform.position.y + 1, pj.transform.position.z));
        }

    }

    void MouseInput()
    {

        if (Input.GetMouseButton(0))
        {
        }
        else if (Input.GetMouseButtonDown(1))
        {
            restartCam = false;
        }
        else if (Input.GetMouseButton(1))
        {
            MouseRightClick();
        }
        else if (Input.GetMouseButtonUp(1))
        {
            ShowAndUnlockCursor();
        }
        else
        {
            MouseWheeling();
        }
    }

    void ShowAndUnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void HideAndLockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void MouseRightClick()
    {
        HideAndLockCursor();
        mainCamera.transform.RotateAround(pj.position, -Vector3.up, Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime);
    }

    void MouseWheeling()
    {
        Vector3 pos = mainCamera.transform.position;
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            pos = pos - mainCamera.transform.forward;
            mainCamera.transform.position = pos;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            pos = pos + mainCamera.transform.forward;
            mainCamera.transform.position = pos;
        }
    }

    public float getActualTime()
    {
        return actualTime;
    }

}
