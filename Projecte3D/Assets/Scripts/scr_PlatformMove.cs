﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PlatformMove : MonoBehaviour
{
    public GameObject player;
    public GameObject Camera;

    [Header("Animation")]
    public Vector3 position1;
    public Vector3 position2;
    public float time;
    public AnimationCurve animationCurve;

    private bool flag;
    
    void Start()
    {
        MoveTo(position2, time);
        flag = false;
    }

    IEnumerator AnimateMove(Vector3 origin, Vector3 target, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            float curvePercent = animationCurve.Evaluate(percent);
            transform.position = Vector3.LerpUnclamped(origin, target, curvePercent);

            yield return null;
        }  

        if (flag)
            MoveTo(position2, duration);
        else
            MoveTo(position1, duration);

        flag = !flag;
    }

    void MoveTo(Vector3 target, float duration)
    {
        StartCoroutine(AnimateMove(transform.position, target, duration));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            player.transform.parent = transform;
            Camera.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            player.transform.parent = null;
            Camera.transform.parent = null;
        }
    }


}
