﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_EndGame : MonoBehaviour
{
    public so_Time soTime;
    public scr_GameController manager;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
            StartCoroutine(ToEndScene());
    }

    IEnumerator ToEndScene()
    {
        yield return new WaitForSeconds(1f);
        soTime.actualTime = manager.getActualTime();
        SceneManager.LoadScene("Endgame");
    }
}
